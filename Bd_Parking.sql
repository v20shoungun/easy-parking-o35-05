-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 28-08-2022 a las 16:26:08
-- Versión del servidor: 8.0.27
-- Versión de PHP: 7.4.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `mydb`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `factura`
--

DROP TABLE IF EXISTS `factura`;
CREATE TABLE IF NOT EXISTS `factura` (
  `idFactura` int NOT NULL,
  `Registro_idRegistroi` int NOT NULL,
  `fecha_fact` datetime DEFAULT NULL,
  `Valor_pagado` int DEFAULT NULL,
  PRIMARY KEY (`idFactura`),
  KEY `fk_Factura_Registro1_idx` (`Registro_idRegistroi`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `plazas_totales`
--

DROP TABLE IF EXISTS `plazas_totales`;
CREATE TABLE IF NOT EXISTS `plazas_totales` (
  `idPlazas_disponibles` int NOT NULL,
  `CantPlaza_bicicletas` int DEFAULT NULL,
  `CantPlaza_motos` int DEFAULT NULL,
  `CantPlaza_automovil` int DEFAULT NULL,
  PRIMARY KEY (`idPlazas_disponibles`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `plaza_disponible`
--

DROP TABLE IF EXISTS `plaza_disponible`;
CREATE TABLE IF NOT EXISTS `plaza_disponible` (
  `idPlaza_disponible` int NOT NULL,
  `Bicicleta_disponible` int DEFAULT NULL,
  `Moto_disponible` int DEFAULT NULL,
  `Automovil_disponible` int DEFAULT NULL,
  PRIMARY KEY (`idPlaza_disponible`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `registro`
--

DROP TABLE IF EXISTS `registro`;
CREATE TABLE IF NOT EXISTS `registro` (
  `idRegistroi` int NOT NULL,
  `Placa` varchar(45) DEFAULT NULL,
  `Registro_ingresocol` datetime DEFAULT NULL,
  `Registro_salida` datetime DEFAULT NULL,
  `Tipo_vehiculo_idTipo_vehiculo` int NOT NULL,
  PRIMARY KEY (`idRegistroi`),
  KEY `fk_Registro_ingreso_Tipo_vehiculo_idx` (`Tipo_vehiculo_idTipo_vehiculo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_vehiculo`
--

DROP TABLE IF EXISTS `tipo_vehiculo`;
CREATE TABLE IF NOT EXISTS `tipo_vehiculo` (
  `idTipo_vehiculo` int NOT NULL,
  `Tipo_vehiculo` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idTipo_vehiculo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `factura`
--
ALTER TABLE `factura`
  ADD CONSTRAINT `fk_Factura_Registro1` FOREIGN KEY (`Registro_idRegistroi`) REFERENCES `registro` (`idRegistroi`);

--
-- Filtros para la tabla `registro`
--
ALTER TABLE `registro`
  ADD CONSTRAINT `fk_Registro_ingreso_Tipo_vehiculo` FOREIGN KEY (`Tipo_vehiculo_idTipo_vehiculo`) REFERENCES `tipo_vehiculo` (`idTipo_vehiculo`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
